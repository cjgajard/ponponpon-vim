# ponponpon-vim

Colorscreme based on Kyary Pamyu Pamyu's song and videoclip.

# Install

With 'vim-plug'.

Add this to your `$MYVIMRC` (e.g. `~/.vimrc` on linux, `%USERPROFILE%\_vimrc` on
windows):
```vim
Plug 'cjgajard/ponponpon-vim'
```
Reload your source calling `so $MYVIMRC`, and install with `:PlugInstall`.
